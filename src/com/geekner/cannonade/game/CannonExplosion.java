package com.geekner.cannonade.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent.PointerCoords;

public class CannonExplosion extends CannonObject {
	private double timer;
	private Paint colorA = new Paint();
	private Paint colorB = new Paint();
	private double startScroll;
	private float scrollDifference;

	public CannonExplosion(float x, float y, CannonGameState state, double time) {
		super(x, y, state.getWidth(), state.getHeight());
		Log.e("EXP","CREATING EXPLOSION: "+x+" "+y+" "+time);
		timer = time;
		size = 0.05f;
		colorA.setColor(Color.rgb(255, 127, 0));
		colorB.setColor(Color.YELLOW);
		startScroll = state.getGround().getScrollPosition();
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawCircle(getPixelX(), getPixelY(), getPixelSize(), (timer-Math.floor(timer)>0.5?colorA:colorB));
	}

	@Override
	public void gameStep(double diffTime, CannonGameState state, boolean touch,	boolean releasedTouch, float pointerX, float pointerY) {
		timer -= diffTime*2;
		scrollDifference = (float)(startScroll-state.getGround().getScrollPosition());
		if(timer < 0.0){
			Log.e("EXP","REMOVING SELF");
			state.removeObject(this);
		}
	}


	@Override
	public int getPixelX() {
		return (int) (super.getPixelX()+scrollDifference*width);
	}

	@Override
	public float getScreenPosX() {
		return getPixelX()/width;
	}
}
