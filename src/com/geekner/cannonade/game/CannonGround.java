package com.geekner.cannonade.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.FloatMath;
import android.view.MotionEvent.PointerCoords;

public class CannonGround extends CannonObject {
	private static final float GRASS_HEIGHT = 0.025f;
	private static final float ROCK_LEVEL = 0.75f;
	
	
	private Paint greenColor = new Paint();
	private Paint brownColor = new Paint();
	private Paint greyColor = new Paint();
	
	private double scrollTime = 7.5;
	
	private double scrollPosition;
	
	public CannonGround(CannonGameState state){
		super(0.0f, 0.0f, state.getWidth(), state.getHeight());
		greenColor.setColor(Color.rgb(50, 175, 25));
		brownColor.setColor(Color.rgb(150, 75, 0));
		greyColor.setColor(Color.DKGRAY);
	}

	@Override
	public void draw(Canvas canvas) {
		for(float ix=0;ix<width;ix++){
			float surface = (float) getPixelGroundHeight(ix);
			canvas.drawLine(ix, surface, ix, height, brownColor);
			if(surface < height*ROCK_LEVEL){
				canvas.drawLine(ix, height*ROCK_LEVEL, ix, surface, greyColor);
			}
			canvas.drawLine(ix, surface+height*GRASS_HEIGHT, ix, surface, greenColor);
		}
	}

	@Override
	public void gameStep(double diffTime, CannonGameState state, boolean touch, boolean releasedTouch, float pointerX, float pointerY) {
		scrollPosition = (scrollPosition+diffTime/scrollTime);
	}
	
	public float getRelativeGroundHeight(float xPos){
		return FloatMath.sin(8.0f*(xPos+(float)scrollPosition))/8.0f+0.33f;
	}
	
	public float getPixelGroundHeight(float xPoint){
		return height*(1.0f-getRelativeGroundHeight(xPoint/width));
	}
	
	public double getScrollPosition(){
		return scrollPosition;
	}
	
	public double getScrollSpeed(){
		return scrollTime;
	}
	
	public void setScrollSpeed(double scroll){
		scrollTime = scroll;
	}

}
