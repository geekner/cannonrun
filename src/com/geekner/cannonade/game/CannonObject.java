package com.geekner.cannonade.game;

import android.graphics.Canvas;
import android.view.MotionEvent.PointerCoords;

public abstract class CannonObject {
	protected float GRAVITY = -9.8f;
	
	protected float posX;//floating point position on screen between 0 and 1
	protected float posY;
	protected float size;
	
	protected float width;
	protected float height;
	
	public CannonObject(float x, float y, float screenWidth, float screenHeight){
		posX = x;
		posY = y;
		width = screenWidth;
		height = screenHeight;
	}
	
	public void setScreenDimensions(float w, float h){
		width = w;
		height = h;
	}
	
	public int getPixelX(){
		return (int) (posX*width);
	}
	
	public int getPixelY(){
		return (int) ((1.0f-posY)*height);
	}
	
	public int getPixelSize(){
		return (int) (size*width);
	}
	
	public float getSize(){
		return size;
	}
	
	public float getScreenPosX(){
		return posX;
	}
	
	public float getScreenPosY(){
		return posY;
	}
	
	public abstract void draw(Canvas canvas);
	public abstract void gameStep(double diffTime, CannonGameState state, boolean touch, boolean releasedTouch, float pointerX, float pointerY);

	public interface CannonCollidable{
		public boolean collisionCompatible(CannonCollidable cc);
	}
}
