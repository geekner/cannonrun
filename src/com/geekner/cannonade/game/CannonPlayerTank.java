package com.geekner.cannonade.game;

import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent.PointerCoords;

public class CannonPlayerTank extends CannonTank {

	@Override
	public void gameStep(double diffTime, CannonGameState state, boolean aTouch, boolean releasedTouch, float pointerX, float pointerY) {
		super.gameStep(diffTime, state, aTouch, releasedTouch, pointerX, pointerY);
		if(aTouch){
			touchX = pointerX;
			touchY = pointerY;
		}
		touch = aTouch;
		float pointX = getPixelX(), pointY = getPixelY();
		cannonAngle = (float) Math.atan2(touchY-pointY,touchX-pointX);
		cannonVelocity = FloatMath.sqrt((float)(Math.pow(touchX-pointX,2)+Math.pow(touchY-pointY,2)))/3.0f;
		if(releasedTouch){
			Log.e("TANK","RELEASED TOUCH: "+cannonAngle + " - d: "+(-cannonAngle*180/Math.PI));
			state.addObject(new CannonBall(-cannonAngle,cannonVelocity,pointX,pointY,state));
		}
	}

	public CannonPlayerTank(CannonGameState state) {
		super(state);
		posX = 0.15f;
		posY = 0.25f;
	}

}
