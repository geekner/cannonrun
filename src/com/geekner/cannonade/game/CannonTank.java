package com.geekner.cannonade.game;

import com.geekner.cannonade.game.CannonObject.CannonCollidable;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent.PointerCoords;

public class CannonTank extends CannonObject implements CannonCollidable {
	protected float touchX;
	protected float touchY;
	protected float angle;
	protected float cannonAngle = 0.1f;
	protected Paint tankColor = new Paint();
	protected Paint barrelColor = new Paint();
	protected Paint barrelColor2 = new Paint();
	protected Paint barrelColor3 = new Paint();
	
	protected float cannonVelocity = 100f;
	
	protected boolean touch = false;
	public CannonTank(CannonGameState state, float sx){
		this(state);
		posX = sx;
	}
	
	public CannonTank(CannonGameState state){
		super(0.75f, 0.25f, state.getWidth(), state.getHeight());
		size = 0.05f;
		tankColor.setColor(Color.rgb(200, 150, 150));
		barrelColor.setColor(Color.WHITE);
		barrelColor2.setColor(Color.RED);
		barrelColor3.setColor(Color.YELLOW);
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.save();
		canvas.translate(getPixelX(), getPixelY());
		canvas.rotate(-angle*30);
		canvas.drawCircle(-width*size/2, width*size/4, size*width/4, tankColor);
		canvas.drawCircle(width*size/2, width*size/4, size*width/4, tankColor);
		canvas.drawRect(-width*size, -width*size/4, width*size, width*size/4, tankColor);
		canvas.save();
		canvas.rotate(angle*30);
		canvas.translate(0, -width*size/4);
		canvas.drawLine(0, 0, FloatMath.cos(cannonAngle)*size*width, FloatMath.sin(cannonAngle)*size*width, barrelColor);
		if(touch && Math.abs(cannonAngle) < Math.PI/2.0){
			float vx = FloatMath.cos(cannonAngle)*cannonVelocity;
			float vy = FloatMath.sin(cannonAngle)*cannonVelocity;
			float timeA, timeB;
			for(float ix=10.0f;ix<width;ix+=10){
				timeA = (ix-5)/vx;
				timeB = ix/vx;
				canvas.drawLine(ix-5, vy*timeA-GRAVITY*timeA*timeA*0.5f, ix, vy*timeB-GRAVITY*timeB*timeB*0.5f, barrelColor3);
			}
		}
		canvas.restore();
		canvas.drawCircle(0, -width*size/4, size*width/4, tankColor);
		canvas.restore();
	}

	@Override
	public void gameStep(double diffTime, CannonGameState state, boolean aTouch, boolean releasedTouch, float pointerX, float pointerY) {
		double left = state.getGround().getRelativeGroundHeight(posX-size/2);
		double right = state.getGround().getRelativeGroundHeight(posX+size/2);
		angle = (float) Math.atan2(right-left,size);
		posY = (float) (right+left)/2+(size/1.33f);
	}

	@Override
	public boolean collisionCompatible(CannonCollidable cc) {
		return true;
	}

}
