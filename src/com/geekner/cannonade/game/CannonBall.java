package com.geekner.cannonade.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent.PointerCoords;

public class CannonBall extends CannonObject {
	private static float GRAVITY = 9.8f;
	
	private float vx;
	private float vy;
	private float[] start = new float[2];
	private float[] point = new float[2];
	private float time = 0.0f;
	private double startScroll;
	private float scrollDifference;
	private Paint ballPaint = new Paint();

	public CannonBall(float cannonAngle, float cannonVelocity, float pointX, float pointY, CannonGameState state) {
		super(pointX/state.getWidth(),pointY/state.getHeight(), state.getWidth(), state.getHeight());
		ballPaint.setColor(Color.MAGENTA);
		vx = FloatMath.cos(cannonAngle)*cannonVelocity;
		vy = FloatMath.sin(cannonAngle)*cannonVelocity;
		start[0]=pointX;
		start[1]=pointY;
		startScroll = state.getGround().getScrollPosition();
		size = 0.01f;
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawCircle(getPixelX(), getPixelY(), size*width, ballPaint);
	}

	@Override
	public void gameStep(double diffTime, CannonGameState state, boolean touch, boolean releasedTouch, float pointerX, float pointerY) {
		time+=diffTime;
		scrollDifference = (float)(startScroll-state.getGround().getScrollPosition());
		simulatePosition(vx,vy,time*10.0f,point);
		CannonObject target = state.findFirstCollision(this, false);
		if(target != null && !(target instanceof CannonPlayerTank)){
			Log.e("CBALL","COLLIDED WITH: "+target.toString());
			state.removeObject(target);
			state.removeObject(this);
			state.addObject(new CannonExplosion(target.getScreenPosX(), target.getScreenPosY(), state, 3.0));
			state.queueNextLevel();
		}else if(getPixelY() > state.getGround().getPixelGroundHeight(getPixelX())){
			state.removeObject(this);
		}
	}
	
	public static float[] simulatePosition(float vx, float vy, float time, float[] data){
		data[0]=vx*time;
		data[1]=vy*time-GRAVITY*time*time*0.5f;
		return data;
	}
	
	public static float simulateHeight(float angle, float velocity, float xPos){
		float vx = FloatMath.cos(angle)*velocity;
		float vy = FloatMath.sin(angle)*velocity;
		float time = xPos/vx;
		return vy*time-GRAVITY*time*time*0.5f;
	}

	@Override
	public int getPixelX() {
		return (int) (start[0]+point[0]+scrollDifference*width);
	}

	@Override
	public int getPixelY() {
		return (int) (start[1]-point[1]);
	}

	@Override
	public float getScreenPosX() {
		return getPixelX()/width;
	}

	@Override
	public float getScreenPosY() {
		return getPixelY()/height;
	}

}
