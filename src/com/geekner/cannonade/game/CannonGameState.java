package com.geekner.cannonade.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.view.MotionEventCompat;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.MotionEvent.PointerCoords;
import android.view.View;
import android.view.View.OnTouchListener;

public class CannonGameState implements OnTouchListener{
	private boolean paused;
	private boolean DEBUG = true;
	
	private long lastUpdateTime;
	
	private ArrayList<CannonObject> gameObjects = new ArrayList<CannonObject>();
	private LinkedList<CannonObject> collideObjects = new LinkedList<CannonObject>();
	private LinkedList<CannonObject> addQueue = new LinkedList<CannonObject>();
	private LinkedList<CannonObject> removeQueue = new LinkedList<CannonObject>();
	private CannonGround ground;
	private CannonPlayerTank tank;
	
	private boolean touchDown = false;
	private boolean releasedTouch = false;
	private float pointerX;
	private float pointerY;
	private Paint testRed = new Paint();
	private Paint testGreen = new Paint();
	
	private float width;
	private float height;
	
	private double levelTimer = 15.0;
	
	private double avgDt;
	private int avgCount;
	private int fps = 0;
	
	private String lockObject = "lockme";
	
	public CannonGameState(){
		ground = new CannonGround(this);
		gameObjects.add(ground);
		tank = new CannonPlayerTank(this);
		gameObjects.add(tank);
		collideObjects.add(tank);
		CannonTank enemy = new CannonTank(this,0.75f);
		gameObjects.add(enemy);
		collideObjects.add(enemy);
		testRed.setColor(Color.rgb(255, 0, 0));
		testGreen.setColor(Color.rgb(0, 255, 0));
	}
	
	public long drawState(Canvas canvas, int width, int height, boolean canAlpha){
		double dt = 0.0;
		synchronized(lockObject){
			if(!paused){
				long currentTimeMillis = System.currentTimeMillis();
				dt = (currentTimeMillis-lastUpdateTime)/1000.0;
				lastUpdateTime = currentTimeMillis;
				levelTimer -= dt;
				if(levelTimer < 0){
					nextLevel();
				}
				for(CannonObject co : gameObjects){
					co.gameStep(dt, this, touchDown, releasedTouch, pointerX, pointerY);
				}
			}
			canvas.drawARGB(255, 0, 0, 255);
			for(CannonObject co : gameObjects){
				co.draw(canvas);
			}
			if(DEBUG){
				canvas.drawCircle(pointerX, pointerY, 20, (touchDown? testRed : testGreen));
				avgDt+=dt;
				avgCount++;
				if(avgCount > 10){
					fps = (int) (1/(avgDt/avgCount)+fps)/2;
					avgDt = 0;
					avgCount = 0;
				}
				canvas.drawText(Integer.toString(fps), width-25, 10, testRed);
			}
			releasedTouch = false;
		}
		while(addQueue.peek() != null){
			CannonObject co = addQueue.poll();
			gameObjects.add(co);
			if(co instanceof CannonObject.CannonCollidable){
				collideObjects.add(co);
			}
		}
		while(removeQueue.peek() != null){
			CannonObject co = removeQueue.poll();
			gameObjects.remove(co);
			collideObjects.remove(co);
		}
		return (paused? 100:0);
	}

	public void resume() {
		paused = false;
		lastUpdateTime = System.currentTimeMillis();
	}
	
	public void pause(){
		paused = true;
	}
	
	private void nextLevel(){
		levelTimer = 15.0;
		CannonObject tank = null;
		for(CannonObject co : collideObjects){
			if(co instanceof CannonTank && !(co instanceof CannonPlayerTank)){
				tank = co;
			}
		}
		if(tank == null){
			CannonTank enemy = new CannonTank(this,0.75f);
			gameObjects.add(enemy);
			collideObjects.add(enemy);
		}
		getGround().setScrollSpeed(getGround().getScrollSpeed()*0.80);
	}

	public CannonObject findFirstCollision(CannonObject collider, boolean targetPlayer) {
		for(CannonObject co : collideObjects){
			if(targetPlayer || !(co instanceof CannonPlayerTank)){
				if(co != collider && dist(co.getPixelX(),co.getPixelY(),collider.getPixelX(),collider.getPixelY()) < co.getPixelSize()+collider.getPixelSize()){
					return co;
				}
			}
		}
		return null;
	}
	
	private float dist(float x1, float y1, float x2, float y2){
		return FloatMath.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		synchronized(lockObject){
			pointerX = event.getX();
			pointerY = event.getY();
			switch(MotionEventCompat.getActionMasked(event)){
			case MotionEvent.ACTION_DOWN:
				touchDown = true;
				releasedTouch = false;
				break;
			case MotionEvent.ACTION_UP:
				touchDown = false;
				releasedTouch = true;
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				touchDown = true;
				releasedTouch = false;
				break;
			case MotionEvent.ACTION_POINTER_UP:
				touchDown = false;
				releasedTouch = true;
				break;
			}
		}
		return true;
	}
	
	public CannonGround getGround(){
		return ground;
	}
	
	public CannonTank getPlayerTank(){
		return tank;
	}
	
	public boolean isTouchDown(){
		return touchDown;
	}
	
	public boolean hasReleasedTouch(){
		return releasedTouch;
	}
	
	public void addObject(CannonObject obj){
		addQueue.add(obj);
	}
	
	public void removeObject(CannonObject obj){
		removeQueue.add(obj);
	}
	
	public float getWidth(){
		return width;
	}
	
	public float getHeight(){
		return height;
	}
	
	public void setDimensions(float w, float h){
		synchronized(lockObject){
			width = w;
			height = h;
			for(CannonObject co : gameObjects){
				co.setScreenDimensions(w, h);
			}
		}
	}

	public void queueNextLevel() {
		levelTimer = 5.0;
	}
}
