package com.geekner.cannonade.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnTouchListener;

public class CannonGameMode implements Runnable, SurfaceHolder.Callback, OnTouchListener {
	private static final String TAG = "CannonGameMode";
	
	private CannonGameState gameState;
	private volatile SurfaceHolder gameView;
	private volatile boolean running;
	private volatile Thread gameLoop;
	
	
	//FPS stuff
	private long targetDelayMilis = 16;//60FPS
	
	//screen size/scale
	private int screenWidth;
	private int screenHeight;
	private boolean canAlpha;
	
	public void onCreate(float scale){
		gameState = new CannonGameState();
	}
	
	public void onResume() {
		gameState.resume();
	}

	@Override
	public void run() {
		while(running && gameView != null){
			long startTime = System.currentTimeMillis();
			Canvas draw = gameView.lockCanvas();
			long additionalDelay = 0;
			if(draw != null){
				//the draw() call will take up the majority of our processing time, up to several milliseconds
				//the game can request additional delay time on top of the existing delay, to reduce processing during pauses and such.
				additionalDelay = gameState.drawState(draw, screenWidth, screenHeight, canAlpha);
				if(running){
					gameView.unlockCanvasAndPost(draw);
				}
			}else{
				additionalDelay = 100;//if draw is null, our surface doesn't exist yet or is locked, give the system more time to create/unlock the surface
			}
			if(running){
				long refreshDelay = targetDelayMilis - (System.currentTimeMillis()-startTime);
				if(refreshDelay > 0){
					try {
						Thread.sleep(refreshDelay+additionalDelay);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}else{
					Thread.yield();
				}
			}
		}
		running = false;
	}

	public void onPause() {
		gameState.pause();
	}
	
	public void onDestroy() {
		
	}
	
	public boolean isRunning(){
		return running;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		screenWidth = width;
		screenHeight = height;
		canAlpha = PixelFormat.formatHasAlpha(format);
		gameState.setDimensions(width, height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		gameView = holder;
		running = true;
		gameLoop = new Thread(this);
		gameLoop.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		running = false;
		try {
			gameLoop.join(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void setHolder(SurfaceHolder holder) {
		gameView = holder;
		gameView.addCallback(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return gameState.onTouch(v, event);
	}

}
