package com.geekner.cannonade;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class CannonGameActivity extends FragmentActivity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cannongame_detail);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.cannongame_detail_container, new CannonGameFragment()).commit();
        }
    }
}
