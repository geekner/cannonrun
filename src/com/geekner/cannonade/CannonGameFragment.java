package com.geekner.cannonade;

import com.geekner.cannonade.game.CannonGameMode;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

public class CannonGameFragment extends Fragment {
    private static final String TAG = "CannonGameFragment";
    
    
    private SurfaceView gameView;
    private CannonGameMode gameState;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameState = new CannonGameMode();
        gameState.onCreate(getScale());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cannongame_detail, container, false);
        gameView = (SurfaceView) rootView.findViewById(R.id.game_surface);
        gameView.setOnTouchListener(gameState);
        SurfaceHolder hold = gameView.getHolder();
        gameState.setHolder(hold);
        return rootView;
    }

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
		gameState.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		gameState.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
	} 

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		gameState.onDestroy();
		gameState = null;
	}

	
	private float getScale(){
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.density;
	}
    
    
}
